import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { API_URL } from 'src/environments/environment';
import { ICliente } from '../interfaces/ICliente';
import { Observable, take } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ClienteServiceService {

  constructor(private httpCliente:HttpClient) {}


  findAll(): Observable<ICliente[]>{
    let httpHeaders = new HttpHeaders()
    .set('Content-Type', 'application/json')
   let options = {
     headers: httpHeaders
   };
    return this.httpCliente.get<ICliente[]>(API_URL,options);
  }

  findById(id:number): Observable<ICliente>{
    let httpHeaders = new HttpHeaders()
    .set('Content-Type', 'application/json')
   let options = {
     headers: httpHeaders
   };
    return this.httpCliente.get<ICliente>(`${API_URL}/${id}`,options);
  }

  save(data:ICliente):Observable<ICliente>{
    let httpHeaders = new HttpHeaders()
     .set('Content-Type', 'application/json')
    let options = {
      headers: httpHeaders
    };
    return this.httpCliente.post<ICliente>(API_URL,JSON.stringify(data),options).pipe(take(1));
  }

  delete(id?:number): Observable<ICliente>{
    let httpHeaders = new HttpHeaders()
    .set('Content-Type', 'application/json')
   let options = {
     headers: httpHeaders
   };
    return this.httpCliente.delete<ICliente>(`${API_URL}/${id}`,options)
  }
}
