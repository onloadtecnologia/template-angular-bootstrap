import { Component, OnInit, Input } from '@angular/core';
import { ICliente } from 'src/app/interfaces/ICliente';
import { ClienteServiceService } from 'src/app/services/cliente-service.service';
import { StoreService } from 'src/app/services/store.service';
import Swal from 'sweetalert2';
import Swall from 'sweetalert2';

@Component({
  selector: 'app-tabela-component',
  templateUrl: './tabela-component.component.html',
  styleUrls: ['./tabela-component.component.css'],
})
export class TabelaComponentComponent implements OnInit {
  @Input() title: string = '';
  clientes: ICliente[] = [];

  constructor(
    private clienteService: ClienteServiceService,
    private store: StoreService
  ) {}

  ngOnInit(): void {
    this.clienteService.findAll().subscribe((data) => (this.clientes = data));
  }

  setCliente(cliente: ICliente) {
    //visite a página sobre e veja o nome do cliente lá
    this.store.setCliente(cliente);

    Swal.fire({
      title: 'Update',
      icon: 'info',
      text: JSON.stringify(cliente),
      footer: 'Aqui você pega os dados para edição Dica use um Modal!',
    });
  }

  delete(id?: number) {
    Swal.fire({
      title: 'Delete',
      icon: 'question',
      text: 'Deseja Realmente Remover?',
      showCancelButton: true,
      showConfirmButton: true,
      footer: new Date().toLocaleDateString('pt-br', {
        day: '2-digit',
        month: '2-digit',
        year: 'numeric',
      }),
    }).then((confirm) => {
      if (confirm.isConfirmed) {
        this.clienteService.delete(id).subscribe()
        return true;
      } else {
        return false;
      }
    })
  }
}
