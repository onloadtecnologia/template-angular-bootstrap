import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { ICliente } from '../interfaces/ICliente';

@Injectable({
  providedIn: 'root'
})
export class StoreService {
  cliente:ICliente={
    id:0,
    nome:"",
    telefone:"",
    email:"",
    cep:""
  }
  constructor() { }

  setCliente(cliente:ICliente){
    return this.cliente=cliente;
  }
  getCliente(){
    return this.cliente;
  }
}
