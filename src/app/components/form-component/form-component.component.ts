import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ICliente } from 'src/app/interfaces/ICliente';
import { ClienteServiceService } from 'src/app/services/cliente-service.service';

@Component({
  selector: 'app-form-component',
  templateUrl: './form-component.component.html',
  styleUrls: ['./form-component.component.css']
})
export class FormComponentComponent implements OnInit {

  constructor(private clienteService:ClienteServiceService,private router:Router) { }

  ngOnInit(): void {
  }

  onSubmit(data:ICliente){
    this.clienteService.save(data).subscribe(
      complete=>{
         this.router.navigate(["clientes"])
      },
      error=>alert(error)
    )

  }

}
