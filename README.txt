Rio de Janeiro 04/06/2022

 Fala Dev, Espero que esteja tudo bem!


1) Na raiz do projeto rode o comando npm install

2) Em seguida rode o comando ng server, caso queira siga as instruções
no arquivo README.md para fazer o build

3) Para ter um Banco Fake Crie um arquivo chamado db.json
fora desta pasta com essas propriedades e este formato

{
  "banco": [
    {
      "nome": "fulano",
      "telefone": "(21)00000-0000",
      "email": "fulanodev@gmail.com",
      "cep": "00000-000",
      "id": 1
    },
    {
      "nome": "fulano",
      "telefone": "(21)00000-0000",
      "email": "fulanodev@gmail.com",
      "cep": "00000-000",
      "id": 2
    },
    {
      "nome": "fulano",
      "telefone": "(21)00000-0000",
      "email": "fulanodev@gmail.com",
      "cep": "00000-000",
      "id": 3
    }
  ]
}

4) Abra o terminal na mesma pasta e rode o comando
json-server --watch db.json





