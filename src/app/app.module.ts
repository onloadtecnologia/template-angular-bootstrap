import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavegadorComponent } from './components/navegador/navegador.component';
import { FormComponentComponent } from './components/form-component/form-component.component';
import { TabelaComponentComponent } from './components/tabela-component/tabela-component.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { HomeComponent } from './views/home/home.component';
import { ClientesComponent } from './views/clientes/clientes.component';
import { StoreModule } from '@ngrx/store';
import { SobreComponent } from './views/sobre/sobre.component';



@NgModule({
  declarations: [
    AppComponent,
    NavegadorComponent,
    FormComponentComponent,
    TabelaComponentComponent,
    HomeComponent,
    ClientesComponent,
    SobreComponent
   ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    StoreModule.forRoot({}, {})
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
