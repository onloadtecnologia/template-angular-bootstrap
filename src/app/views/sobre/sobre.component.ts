import { Component, OnInit } from '@angular/core';
import { ICliente } from 'src/app/interfaces/ICliente';
import { StoreService } from 'src/app/services/store.service';

@Component({
  selector: 'app-sobre',
  templateUrl: './sobre.component.html',
  styleUrls: ['./sobre.component.css']
})
export class SobreComponent implements OnInit {

  cliente!: ICliente;
  constructor(private store : StoreService) { }

  ngOnInit(): void {
    this.cliente=this.store.getCliente();
  }

}
